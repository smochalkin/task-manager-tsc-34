package ru.smochalkin.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    private final int interval = 3;

    @NotNull
    private final String path = "./";

    @NotNull
    public final Bootstrap bootstrap;

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        for (@NotNull final AbstractCommand command : bootstrap.getCommandService().getArguments()) {
            commands.add(command.name());
        }
        es.scheduleWithFixedDelay(this, interval, interval, TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        @NotNull final File file = new File(path);
        for (File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            if (!commands.contains(fileName)) continue;
            try {
                System.out.println("::FILE SCANNER START::");
                bootstrap.parseCommand(fileName);
            } catch (Exception e) {
                bootstrap.getLogService().info("File scanner info: " + e.getMessage());
            } finally {
                item.delete();
                System.out.println("::FILE SCANNER END::");
                System.out.println();
            }
        }
    }

}
