package ru.smochalkin.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.Domain;

public interface IDomainService {
    @NotNull
    @SneakyThrows
    Domain getDomain();

    @SneakyThrows
    void setDomain(@Nullable Domain domain);

    @SneakyThrows
    void loadBase64();

    @SneakyThrows
    void saveBase64();

    @SneakyThrows
    void loadBinary();

    @SneakyThrows
    void saveBinary();

    @SneakyThrows
    void loadBackup();

    @SneakyThrows
    void saveBackup();

    @SneakyThrows
    void loadJsonFasterXml();

    @SneakyThrows
    void saveJsonFasterXml();

    @SneakyThrows
    void loadXmlFasterXml();

    @SneakyThrows
    void saveXmlFasterXml();

    @SneakyThrows
    void loadYamlFasterXml();

    @SneakyThrows
    void saveYamlFasterXml();

    @SneakyThrows
    void loadJsonJaxb();

    @SneakyThrows
    void saveJsonJaxb();

    @SneakyThrows
    void loadXmlJaxb();

    @SneakyThrows
    void saveXmlJaxb();
}
