package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperName();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull String getSignSecret();

    @NotNull Integer getSignIteration();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();
}
