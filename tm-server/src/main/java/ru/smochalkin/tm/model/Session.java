package ru.smochalkin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity implements Cloneable {

    @NotNull
    private String userId;

    @Nullable
    private String signature;

    private long timestamp = System.currentTimeMillis();

    public Session(@NotNull final String userId) {
        this.userId = userId;
    }

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
